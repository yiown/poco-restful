#ifndef GLOBALS_H
#define GLOBALS_H
#include <Poco/Util/PropertyFileConfiguration.h>

/** @brief Configuration provider. */
extern Poco::AutoPtr<Poco::Util::PropertyFileConfiguration> configs;

#endif // GLOBALS_H
