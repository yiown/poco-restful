#include <base/Globals.h>

/** @brief Configuration provider as a property file configuration. */
Poco::AutoPtr<Poco::Util::PropertyFileConfiguration> configs;
